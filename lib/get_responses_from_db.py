from pymongo import MongoClient
import json
import re
from env import *
import random
import requests
import re
import logging
logging.getLogger('').handlers = []
logging.basicConfig(format='%(asctime)s.%(msecs)03d %(levelname)s %(message)s',level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')

def fetch_faq_response_api(group_id,intent_name,entity_name,entity_value,platform,sender_id):
    group_id = "613ef78264fe23751f252227"
    headers = {
        "Content-Type": "application/json",
        "authorization": FAQ_AUTHORIZATION
    }
    if entity_name is not None:
        body = { 
            "data": {
                "_action": "intentEntityResponse", 
                "_param": { 
                    "group_id": group_id, 
                    "intent_name": intent_name, 
                    "entity_name": entity_name, 
                    "entity_value": entity_value, 
                    "language": "en",
                    "platform": "Web",
                    "session_id" : sender_id
                } 
            } 
        }
    else:
        body = { 
            "data": {
                "_action": "intentEntityResponse", 
                "_param": { 
                    "group_id": group_id,
                    "intent_name": intent_name, 
                    "language": "en",
                    "platform": "Web",
                    "session_id" : sender_id
                } 
            } 
        }
    try:
        logging.info("session_id:" + sender_id + "~rasa_cms_request~" + json.dumps(body))
        response = requests.post(url=FAQ_API_URI, data=json.dumps(body), headers=headers)
        data = response.json()
        logging.info("session_id:" + sender_id + "~rasa_cms_response~" + json.dumps(data))
        return data
    except:
        return None